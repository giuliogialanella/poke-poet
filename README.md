# Poke-poet

Poke-poet is a REST api that, given the name of a Pokémon, returns its description in
Shakespearean English.

## Endpoints

### Get Pokemon Description
- **Route** `/pokemon/<pokemon-name>`
- **Method**: `GET`
- **Sample request**:
`curl localhost:8080/pokemon/squirtle`
- **Sample Response**:
```json
{
    'name': 'squirtle',
    'description': 'Squirtle’s shell is not merely did lay-to for protection. The shell’s rounded shape and the grooves 
                    on its surface holp minimize resistance in water,  enabling this pokémon to swim at high speeds.',
}
```

## How to run Locally

- **Requirements**: Java 8

- **Command**: launch from the `poke-poet` path

    `./mvnw spring-boot:run`

The endpoint will be exposed on `localhost:8080`

## Run locally with Docker
- **Requirements**: Java 8, Docker
- **Commands**: launch from the `poke-poet` path

```shell script
    ./mvnw clean install 
    docker build -t poke-poet .
    docker run -p 8080:8080 poke-poet
```
