package com.example.pokepoet.service;

public interface PokeService {
    String getFlavorText(String name);
}
