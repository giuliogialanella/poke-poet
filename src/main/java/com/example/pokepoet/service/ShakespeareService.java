package com.example.pokepoet.service;

public interface ShakespeareService {
    String translate(String text);
}
