package com.example.pokepoet.service;

import com.example.pokepoet.model.pokemon.FlavorTextEntry;
import com.example.pokepoet.model.pokemon.PokemonSpecies;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@Service
public class PokeServiceImpl implements PokeService{
    final static String POKE_POET = "poke-poet";

    @Value("${pokeapi.url}")
    String url;

    RestTemplate restTemplate;

    public PokeServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private PokemonSpecies getPokemonSpecies(String name) throws HttpClientErrorException {
        log.info("Getting pokemon species: " + name);
        HttpEntity<String> request = buildRequestEntity();
        ResponseEntity<PokemonSpecies> pokemonSpeciesResponseEntity =
                    restTemplate.exchange(url + name, HttpMethod.GET, request, PokemonSpecies.class);
        return pokemonSpeciesResponseEntity.getBody();
    }

    private HttpEntity<String> buildRequestEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(HttpHeaders.USER_AGENT, POKE_POET);
        return new HttpEntity<>("parameters", headers);
    }

    private String findLongestEnglishFlavorText(List<FlavorTextEntry> flavorTextEntries) throws NoSuchElementException {
        String text = flavorTextEntries.stream()
                .filter(f -> f.getLanguage().getName().equals("en"))
                .map(FlavorTextEntry::getFlavorText)
                .max(Comparator.comparing(String::length)).orElseThrow(() -> new NoSuchElementException("Flavor text not available."));

        return text.replaceAll("\\s", " ");
    }

    @Cacheable("pokemon")
    public String getFlavorText(String name) {
        PokemonSpecies pokemonSpecies = getPokemonSpecies(name);
        return findLongestEnglishFlavorText(pokemonSpecies.getFlavorTextEntries());
    }

}
