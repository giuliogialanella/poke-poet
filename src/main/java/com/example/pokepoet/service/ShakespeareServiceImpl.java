package com.example.pokepoet.service;

import com.example.pokepoet.model.shakespeare.ShakespeareResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
@Service
public class ShakespeareServiceImpl implements ShakespeareService{
    private static final String POKE_POET = "poke-poet";

    RestTemplate restTemplate;
    @Value("${shakespeare.url}")
    String url;

    @Autowired
    public ShakespeareServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Cacheable("shakespeare")
    public String translate(String text) {
        log.info("Translating to shakespeare: " + text);
        HttpEntity<MultiValueMap<String, String>> request = buildRequestEntity(text);
        ShakespeareResponse shakespeareResponse =
                restTemplate.postForObject(url, request, ShakespeareResponse.class);
        return shakespeareResponse.getContents().getTranslated().get(0);
    }

    private HttpEntity<MultiValueMap<String, String>> buildRequestEntity (String text) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(HttpHeaders.USER_AGENT, POKE_POET);
        headers.setContentType(MediaType.APPLICATION_JSON);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("text", text);
        return new HttpEntity<>(map, headers);
    }
}
