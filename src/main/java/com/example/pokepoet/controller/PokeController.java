package com.example.pokepoet.controller;

import com.example.pokepoet.model.ErrorResponse;
import com.example.pokepoet.model.PokePoetResponse;
import com.example.pokepoet.service.PokeService;
import com.example.pokepoet.service.ShakespeareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.NoSuchElementException;

@RestController
public class PokeController {

    @Autowired
    PokeService pokeService;
    @Autowired
    ShakespeareService shakespeareService;

    @GetMapping("/pokemon/{name}")
    ResponseEntity<?> getPokemon(@PathVariable String name) {
        try {
            String flavorText = pokeService.getFlavorText(name);
            try {
                String shakespeare = shakespeareService.translate(flavorText);
                return new ResponseEntity<>(new PokePoetResponse(name, shakespeare), HttpStatus.OK);
            } catch (HttpClientErrorException e){
                int statusCode = e.getRawStatusCode();
                String message = e.getMessage();
                return new ResponseEntity<>(new ErrorResponse(statusCode, message), HttpStatus.valueOf(statusCode));
            }
        } catch (HttpClientErrorException e) {
            int statusCode = e.getRawStatusCode();
            String message = e.getMessage();
            return new ResponseEntity<>(new ErrorResponse(statusCode, message), HttpStatus.valueOf(statusCode));
        } catch (NoSuchElementException e) {
            int statusCode = 404;
            String message = e.getMessage();
            return new ResponseEntity<>(new ErrorResponse(404, message), HttpStatus.valueOf(statusCode));
        }
    }
}
