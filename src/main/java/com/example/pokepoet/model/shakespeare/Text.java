package com.example.pokepoet.model.shakespeare;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Text {
    String text;
}
