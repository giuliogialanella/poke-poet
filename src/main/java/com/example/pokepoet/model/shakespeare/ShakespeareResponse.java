package com.example.pokepoet.model.shakespeare;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShakespeareResponse {
    Success success;
    Contents contents;
}
