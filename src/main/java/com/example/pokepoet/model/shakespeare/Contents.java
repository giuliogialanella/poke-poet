package com.example.pokepoet.model.shakespeare;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contents {
    List<String> translated;
    List<String> text;
    String translation;
}
