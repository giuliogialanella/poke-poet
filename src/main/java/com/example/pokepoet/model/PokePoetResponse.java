package com.example.pokepoet.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PokePoetResponse {
    String name;
    String description;
}
