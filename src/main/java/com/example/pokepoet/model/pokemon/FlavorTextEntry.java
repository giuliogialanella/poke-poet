package com.example.pokepoet.model.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlavorTextEntry {
    @JsonProperty("flavor_text")
    String flavorText;
    Language language;
}



