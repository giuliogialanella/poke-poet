package com.example.pokepoet;

import com.example.pokepoet.model.pokemon.FlavorTextEntry;
import com.example.pokepoet.model.pokemon.Language;
import com.example.pokepoet.model.pokemon.PokemonSpecies;
import com.example.pokepoet.service.PokeService;
import com.example.pokepoet.service.PokeServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PokeServiceTest {

    @Test
    public void shouldGetCorrectFlavorText() {
        List<FlavorTextEntry> flavorTextEntries = new ArrayList<>();
        flavorTextEntries.add(new FlavorTextEntry("short english flavor text", new Language("en", "test")));
        flavorTextEntries.add(new FlavorTextEntry("long italian flavor text aaa", new Language("it", "test")));
        flavorTextEntries.add(new FlavorTextEntry("long english flavor text aa", new Language("en", "test")));
        PokemonSpecies pokemonSpecies = new PokemonSpecies("charizard", flavorTextEntries);
        ResponseEntity<PokemonSpecies> pokemonSpeciesEntity = new ResponseEntity<>(pokemonSpecies, HttpStatus.OK);

        RestTemplate restTemplate = mock(RestTemplate.class);
        when(restTemplate.exchange(eq("pokeapi-url/test"), any(HttpMethod.class), any(HttpEntity.class), eq(PokemonSpecies.class)))
        .thenReturn(pokemonSpeciesEntity);
        PokeService pokeService = new PokeServiceImpl(restTemplate);
        ReflectionTestUtils.setField(pokeService, "url", "pokeapi-url/");

        String flavorText = pokeService.getFlavorText("test");
        assertEquals("long english flavor text aa", flavorText);
    }
}
