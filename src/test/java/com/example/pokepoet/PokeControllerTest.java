package com.example.pokepoet;

import com.example.pokepoet.service.PokeService;
import com.example.pokepoet.service.ShakespeareService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class PokeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PokeService pokeService;

    @MockBean
    private ShakespeareService shakespeareService;

    @Test
    public void whenValidInput_thenReturns200() throws Exception {
        Mockito.when(pokeService.getFlavorText("charizard"))
                .thenReturn("Charizard text");
        Mockito.when(shakespeareService.translate("Charizard text"))
                .thenReturn("Shakespearizard");
        mockMvc.perform(get("/pokemon/charizard")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenInvalidInput_thenReturns404() throws Exception {
        Mockito.when(pokeService.getFlavorText("carrot"))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));
        mockMvc.perform(get("/pokemon/carrot")
                .contentType("application/json"))
                .andExpect(status().is4xxClientError());
    }
}
