package com.example.pokepoet;

import com.example.pokepoet.model.shakespeare.Contents;
import com.example.pokepoet.model.shakespeare.ShakespeareResponse;
import com.example.pokepoet.model.shakespeare.Success;
import com.example.pokepoet.service.ShakespeareService;
import com.example.pokepoet.service.ShakespeareServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ShakespeareServiceTest {
    @Test
    public void shouldTranslateString() {
        Success success = new Success(1);
        Contents contents = new Contents(
                Collections.singletonList("translated"),
                Collections.singletonList("text"),
                "shakespeare"
        );
        ShakespeareResponse shakespeareResponse = new ShakespeareResponse(success, contents);
        RestTemplate restTemplate = mock(RestTemplate.class);
        when(restTemplate.postForObject(eq("shakespeare-url"), any(HttpEntity.class), eq(ShakespeareResponse.class)))
                .thenReturn(shakespeareResponse);
        ShakespeareService shakespeareService = new ShakespeareServiceImpl(restTemplate);
        ReflectionTestUtils.setField(shakespeareService, "url", "shakespeare-url");

        String translated = shakespeareService.translate("text");
        assertEquals("translated", translated);
    }
}
